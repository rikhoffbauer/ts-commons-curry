module.exports = function(w) {
    return {
        maxConsoleMessagesPerTest: 10000,

        files: [
            'src/**/*',
            'test/**/*',
            'generated/**/*',
            'scripts/**/*',
            '!test/**/*.spec.ts',
            '!test/**/*.spec.tsx',
            'jest.config.json',
            'package.json',
            'tsconfig.json',
        ],

        tests: [
            'test/**/*.spec.ts',
            'test/**/*.spec.tsx'
        ],

        env: {
            type: 'node',
            runner: 'node'
        },

        testFramework: 'jest',

        setup: function(wallaby) {
            var jestConfig = require('./jest.config.json');
            jestConfig.globals = { "__DEV__": true };
            wallaby.testFramework.configure(jestConfig);
        },

        compilers: {
            '**/*.ts': w.compilers.typeScript(),
            '**/*.tsx': w.compilers.typeScript()
        }
    };
};
