import { execSync } from "child_process";
import { join } from "path";
import { Configuration } from "webpack";

const TypedocWebpackPlugin = require('typedoc-webpack-plugin');

const CleanWebpackPlugin = require("clean-webpack-plugin");
const WebpackShellPlugin = require("webpack-shell-plugin");
const getPath = (...segments: string[]) => join(__dirname, ...segments);

const paths = {
    entry: getPath("src/index.ts"),
    outDir: getPath("lib"),
    outFile: "[name].js",
};

// TODO find a better way to incorporate this into the webpack build
// webpack-shell-plugin doesn't seem to wait for the command to finish before
// starting the webpack build...
execSync(`npm run generate-types`);

const config: Configuration = {
    entry: paths.entry,
    devtool: "source-map",
    mode: "production",
    target: "node",
    output: {
        path: paths.outDir,
        libraryTarget: "commonjs2",
        filename: paths.outFile,
    },
    resolve: {
        extensions: [".ts", ".js", ".json"],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "awesome-typescript-loader",
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin([
            "lib",
            "js",
            "docs",
        ]),
        new WebpackShellPlugin({
            safe: true,
            onBuildEnd: ["rm -rf js && node_modules/.bin/tsc"],
        }),
        new TypedocWebpackPlugin({
            name: '@ts-commons/curry',
            mode: 'modules',
            out: "../docs",
            theme: './node_modules/typedoc-clarity-theme/bin',
            exclude: ['**/node_modules/**/*.*', '**/src/index.ts'],
            includeDeclarations: true,
            ignoreCompilerErrors: true,
        }, ["./src", "./generated"]),
    ],
};

export default config;
