# @ts-commons/curry 🌶

![coverage](https://gitlab.com/rikhoffbauer/ts-commons-curry/badges/master/coverage.svg)
![build](https://gitlab.com/rikhoffbauer/ts-commons-curry/badges/master/build.svg)
![npm version](https://badge.fury.io/js/%40ts-commons%2Fcurry.svg)

A curry function that leverages TypeScripts type inference.

## Installing

##### Using npm

`npm install @ts-commons/curry`

##### Using yarn

`yarn add @ts-commons/curry`

## Usage

```typescript
import curry, { __ } from "@ts-commons/curry";

const add = curry((a: number, b: number) => a + b);

add(1, 2); // 3
add(1)(2); // 3

// you can provide a placeholder (__) to temporarily skip an argument
add(__, 2)(1); // 3

```

## Usage with functions with generics
Due to limitations in TypeScripts type inference the types of functions with generics can't be inferred.
```typescript
import curry, { __ } from "@ts-commons/curry";

const map = <A, B>(fn: (a: A) => B, arr: A[]): B[] => arr.map(fn);

// throws type errors
curry(map);
```

You can work around this issue like so:
```typescript
type MapFn<A, B> = (a: A) => B;
const map = <A, B>(fn: MapFn<A, B>, arr: A[]): B[] => arr.map(fn);
// doesn't throw errors and has expected signature and return type
curry<string[], MapFn<number, string>, number[]>(map);
```
or
```typescript
const map = <A, B>() => (fn: (a: A) => B, arr: A[]): B[] => arr.map(fn);
// doesn't throw errors and has expected signature and return type
curry(map<number, string>());
```

This is obviously not ideal but at the moment there does not seem to be a way around it.

## Contributing

Contributions in the form of pull requests and bug reports are appreciated.

### Running tests

Tests are ran using the `test` npm script.

##### Using npm

`npm test`

##### Using yarn

`yarn test`
