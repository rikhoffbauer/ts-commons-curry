import { append, indent, pipe, prepend, range } from "./helpers";
import { FunctionSignature, Interface, Type } from "./ts-code-generators";

const IsPlaceholderType = (
    generic: string,
    trueCase: string,
    falseCase: string,
) => `IsPlaceholder<${generic}, ${trueCase}, ${falseCase}>`;

const CurriedType = (argGenerics: string[]) =>
    argGenerics.length
        ? `Curried${argGenerics.length}<R, ${argGenerics.join(", ")}>`
        : `R`;

const generatePlaceholderType = () =>
    Type({
        name: "Placeholder",
        value: `{ __curry_placeholder: "PLACEHOLDER" }`,
    });

const generateIsPlaceholderType = () =>
    Type({
        name: "IsPlaceholder",
        generics: ["T", "True", "False"],
        value: "T extends Placeholder ? True : False",
    });

const generateCurryOverload = (argumentCount: number) => {
    const { generics, args } = range(1, argumentCount).reduce(
        ({ generics, args }, i) => ({
            generics: [...generics, `A${i}`],
            args: [...args, `a${i}: A${i}`],
        }),
        { generics: [`R`], args: [] as string[] },
    );
    const genericsStr = generics.join(", ");

    return (
        `<${genericsStr}>(fn: (${args.join(", ")}) => R)` +
        `: Curried${argumentCount}<${genericsStr}>`
    );
};
const generateCurryInterface = (argumentCount: number) => {
    const overloads = range(1, argumentCount)
        .map(pipe(generateCurryOverload, indent(4), append(";\n")))
        .join("");

    return `export interface Curry {\n` + overloads + `}\n`;
};

const generateCurriedTypes = (argumentCount: number) =>
    range(1, argumentCount)
        .map(pipe(generateCurriedType, append("\n")))
        .join("");

const generateCurriedType = (argCount: number) => {
    if (argCount <= 1) {
        return Interface({
            name: `Curried1`,
            generics: [`R`, `A1`],
            properties: [`(a1: A1): R`],
        });
    }

    const generateOverload = generateCurriedTypeOverload.bind(null, argCount);

    return Interface({
        name: `Curried${argCount}`,
        generics: [`R`, ...range(1, argCount).map(pipe(prepend("A")))],
        properties: range(1, argCount).map(generateOverload),
    });
};

const generateCurriedTypeOverload = (
    totalArgs: number,
    argumentCount: number,
) => {
    const argGenericNames = range(1, totalArgs).map(i => `A${i}`);
    const { lastArg, args, genericNames, generics } = range(
        1,
        argumentCount,
    ).reduce(
        ({ genericNames, generics, args, lastArg }, i) => ({
            args: [...args, `a${i}: T${i}`],
            lastArg: `a${i}: T${i}`,
            genericNames: [...genericNames, `T${i}`],
            generics: [...generics, `T${i} extends A${i} | Placeholder`],
        }),
        {
            generics: [] as string[],
            args: [] as string[],
            lastArg: "",
            genericNames: [] as string[],
        },
    );

    const returnType = generateCurriedReturnType(
        totalArgs,
        lastArg,
        genericNames,
        argGenericNames,
    );

    return FunctionSignature({ generics: generics, args, returnType });
};

const generateCurriedReturnType = (
    argumentCount: number,
    lastArg: string,
    placeholderGenerics: string[],
    argGenerics: string[],
    originalArgs = argGenerics,
): string => {
    if (!placeholderGenerics.length) {
        return CurriedType(
            argGenerics === originalArgs ? argGenerics.slice(1) : argGenerics,
        );
    }

    const generic = placeholderGenerics[0];

    return IsPlaceholderType(
        generic,
        generateCurriedReturnType(
            argumentCount,
            lastArg,
            placeholderGenerics.slice(1),
            argGenerics.filter(v => v !== lastArg),
            originalArgs,
        ),
        generateCurriedReturnType(
            argumentCount,
            lastArg,
            placeholderGenerics.slice(1),
            argGenerics.filter(
                v => v !== generic.replace("T", "A") && v !== lastArg,
            ),
            originalArgs,
        ),
    );
};

export default (argumentCount: number) =>
    [
        generatePlaceholderType(),
        generateIsPlaceholderType(),
        generateCurryInterface(argumentCount),
        generateCurriedTypes(argumentCount),
    ].join("\n");
