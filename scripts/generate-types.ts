import { writeFile } from "fs";
import mkdirp from "mkdirp";
import generateCurryTypes from "./generate-curry-types";
import { dirname, join } from "path";

const curryDst = join(__dirname, "../generated/curry.ts");

mkdirp(dirname(curryDst), err => {
    if (err)
        return console.error(
            `Failed to create curry type definitions`,
            err.message,
        );

    writeFile(curryDst, generateCurryTypes(10), "utf-8", err => {
        if (err)
            return console.error(
                `Failed to create curry type definitions`,
                err.message,
            );

        console.log(`Successfully created curry type definitions`);
    });
});

