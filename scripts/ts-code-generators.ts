export interface FunctionSignatureOptions {
    generics?: string[];
    args?: string[];
    returnType?: string;
}

export const FunctionSignature = ({
    generics = [],
    args = [],
    returnType,
}: FunctionSignatureOptions) =>
    `${
        generics && generics.length
            ? `<${generics.filter(Boolean).join(", ")}>`
            : ``
    }(${args.join(", ")}): ${returnType}`;

export interface InterfaceOptions {
    name: string;
    properties: string[];
    export?: boolean;
    generics?: string[];
}

export const Interface = ({
    generics,
    name,
    properties,
    export: exp = true,
}: InterfaceOptions): string =>
    `${exp ? `export ` : ``}interface ${name}${
        generics && generics.length
            ? `<${generics.filter(Boolean).join(", ")}>`
            : ``
    } {\n    ${properties
        .map(v => v.replace(/;/g, ""))
        .join(";\n    ")};\n}\n`.replace(/;+/g, ";");

export interface TypeOptions {
    name: string;
    value: string;
    export?: boolean;
    generics?: string[];
}

export const Type = ({
    generics,
    name,
    value,
    export: exp = true,
}: TypeOptions): string =>
    `${exp ? `export ` : ``}type ${name}${
        generics && generics.length
            ? `<${generics.filter(Boolean).join(", ")}>`
            : ``
    }  = ${value};\n`.replace(/;+/g, ";");
